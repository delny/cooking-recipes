<?php

namespace App\DataFixtures;

use App\Entity\RecipeStep;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RecipeStepFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        for ($i=0; $i <= 11; $i++) {
            $recipeStep = new RecipeStep();
            $recipeStep->setBody('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque eu commodo orci.');

            $k = ($i %3) + 1;
            $recipeStep->setRecipe($this->getReference('recette-'. $k));

            $manager->persist($recipeStep);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RecipeFixtures::class,
        ];
    }
}
