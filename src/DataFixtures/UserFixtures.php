<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager): void
    {
        $data = [
            [
                'email' => 'toto@local.lan',
                'password' => 'toto',
            ],
            [
                'email' => 'admin@local.lan',
                'password' => 'admin',
            ],
        ];

        foreach ($data as $item) {
            $user = new User();
            $user->setEmail($item['email']);
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $item['password']
            ));
            $user->setRoles(['ROLE_ADMIN']);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
