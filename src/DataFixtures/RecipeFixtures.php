<?php

namespace App\DataFixtures;

use App\Entity\Recipe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RecipeFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i=1; $i <= 4; $i++) {
            $recipe = new Recipe();
            $recipe->setTitle('Recette '.$i);
            $recipe->setPreparationTime(rand(20, 120));
            $recipe->setCookingTime(rand(15, 65));
            $recipe->setPeopleNumber(rand(1, 5));
            $recipe->setDifficulty(rand(1, 3));
            $recipe->setResume('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id mauris euismod, venenatis velit a, semper nibh. Fusce auctor, libero ut sollicitudin congue, ligula nunc ullamcorper quam, ut consectetur dui quam a dui. Cras ornare dictum arcu. In hac habitasse platea dictumst. Nulla sagittis libero lacus, non vehicula odio venenatis non. Ut et commodo tellus, et posuere urna. Nam egestas ipsum tellus, sagittis iaculis velit lobortis non. Praesent venenatis urna sed sem tempus, in malesuada tellus sodales. Proin congue purus sit amet velit porta eleifend. Morbi et dui in tellus ultricies hendrerit. Suspendisse ac eros aliquet, varius arcu ac, dapibus nibh.');

            $this->addReference('recette-'.$i, $recipe);
            $manager->persist($recipe);
        }

        $manager->flush();
    }
}
