<?php

namespace App\DataFixtures;

use App\Entity\RecipeIngredient;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RecipeIngredientFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $unities = ['mg', 'g', 'kg', 'L', 'cL'];

        for ($i=0; $i <= 11; $i++) {
            $recipeIngredient = new RecipeIngredient();
            $j = $i + 1;
            $recipeIngredient->setName('ingredient '. $j);
            $recipeIngredient->setQuantity(rand(5, 15));
            $recipeIngredient->setUnity($unities[rand(0, 4)]);

            $k = ($i %3) + 1;
            $recipeIngredient->setRecipe($this->getReference('recette-'. $k));

            $manager->persist($recipeIngredient);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RecipeFixtures::class,
        ];
    }
}
