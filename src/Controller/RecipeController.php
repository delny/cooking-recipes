<?php

namespace App\Controller;

use App\Entity\Recipe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 */
class RecipeController extends AbstractController
{
    /**
     * @Route("/recette/{id}", name="recipe_detail")
     */
    public function RecipeDetail(Recipe $recipe): Response
    {
        return $this->render('recipes/detail.html.twig', [
      'recipe' => $recipe,
    ]);
    }
}
