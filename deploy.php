<?php
namespace Deployer;

require 'recipe/symfony4.php';

// Project name
set('application', 'Rcettes');
set('directory', 'cooking_recipes');
set('bin/php', function () {
    return '/usr/local/php7.4/bin/php';
});
set('bin_dir', 'bin');

// Project repository
set('repository', 'git@gitlab.com:delny/cooking-recipes.git');

// Shared files/dirs between deploys 
set('shared_files', ['.env.local']);
set('shared_dirs', []);

// Writable dirs by web server 
set('writable_dirs', []);

// Hosts
inventory('deploy/hosts.yml'); 

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'database:migrate');

